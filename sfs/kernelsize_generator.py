"""Base kernelsize generator

The job of the kernelsize generator is to create a collection of kernel sizes.
"""

#TODO needs assertions?

from abc import ABCMeta
from abc import abstractmethod

import tensorflow as tf

class KernelsizeGenerator(object):
  """Abstract base class for kernelsize generator."""
  __metaclaass__ = ABCMeta

  @abstractmethod
  def name_scope(self):
    """Name scope.
    """
    pass

  def generate(self, feature_map_shape_list, **params):
    """Generates a collection of kernel sizes.

    Args:
      feature_map_shape_list: list of (height, width) pairs in the format
        [(height_0, width_0), (height_1, width_1), ...] that the generated
        kernel sizes must align with.
      **params: parameters for kernel size generator

    Returns:
      kernels: a list of kernel sizes
    """
    #TODO: numpy or tf? scoping?
    #with tf.name_scope(self.name_scope()):
    #  kernel_sizes = self._generate(feature_map_shape_list)
    return self._generate(feature_map_shape_list, **params)

  @abstractmethod
  def _generate(self, feature_map_shape_list, **params):
    """To be overridden.

    Args:
      feature_map_shape_list: list of (height, width) pairs in the format
        [(height_0, width_0), (height_1, width_1), ...] that the generated
        kernel sizes must align with.
      **params: parameters for kernel size generator

    Returns:
      kernel_sizes: a list of kernels sizes
    """
    pass

