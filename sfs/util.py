import tensorflow as tf
from tensorflow.contrib.framework.python.ops import add_arg_scope
import numpy as np
import copy
slim = tf.contrib.slim

def tfmap(fn, inputs, params, name=None):
  return tf.map_fn(lambda x: fn(*x), (inputs, params), dtype=inputs.dtype, name=name)

@add_arg_scope
def dynamic_conv2d(
        inputs,
        kernels,
        biases=None,
        rate=1,
        padding='SAME',
        data_format=None,
        activation_fn=tf.nn.relu,
        normalizer_fn=None,
        normalizer_params=None,
        scope=None):

  ''''
    dynamic 2d convolution implementation
    Args:
      inputs: A tensor of shape [meta_batch_size, batch_size, h, w, in]
      kernels: A tensor of shape [meta_batch_size, kh, kw, in, out]
      biases shape [meta_batch_size, out]

    Returns:
      ouptut: A tensor of shape [meta_batch_size, batch_size, h, w, out] if rate=1

  '''
  assert(kernels.shape[1:].is_fully_defined()), 'Kernel shape should be fully defined'
  assert(len(kernels.shape) == 5), 'kernel should be 5 dimensional (meta_batch_size, kh, kw, out, in)'
  assert(len(inputs.shape) == 5), 'inputs should be 5 dimensional (meta_batch_size, batch_size, h, w, in)'

  if kernels.shape[0].value and inputs.shape[0].value:
      assert(kernels.shape[0] == inputs.shape[0]), 'Kernel and input meta batch size mismatch'
  else:
      tf.assert_equal(tf.shape(kernels)[0],
          tf.shape(inputs)[0], message='Kernel and input meta batch size mismatch')

  if data_format not in [None, 'NHWC']:
    raise ValueError('Invalid data_format: %r' % (data_format,))

  with tf.name_scope(scope, 'dynamic_conv2d', [inputs, kernels, biases]) as scope:
    inputs = tf.convert_to_tensor(inputs)
    kernels = tf.convert_to_tensor(kernels)
    if rate > 1:
      conv2d = lambda inputs, kernel: tf.nn.atrous_conv2d(inputs, kernel, rate, padding=padding)
    else:
      strides = [1]*4
      conv2d = lambda inputs, kernel: tf.nn.conv2d(inputs, kernel, strides, padding=padding)
    outputs = tfmap(conv2d, inputs, kernels, name='conv2d')

    if biases is not None and normalizer_fn is None:
      biases = tf.convert_to_tensor(biases)
      add_bias = lambda inputs, biases: tf.nn.bias_add(inputs, biases)
      outputs = tfmap(add_bias, outputs, biases, name='biases')

    if normalizer_fn is not None:
      normalizer_params = normalizer_params or {}
      outputs = normalizer_fn(outputs, **normalizer_params)

    if activation_fn is not None:
        outputs = activation_fn(outputs)
    return outputs


