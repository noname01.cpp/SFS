"""Generates grid of maxpool kernel sizes.
"""

import tensorflow as tf
from sfs_faster_rcnn.sfs import kernelsize_generator

class GridKernelsizeGenerator(kernelsize_generator.KernelsizeGenerator):
  """Generates a grid of kernel sizes given a set of scales and aspects.
  """

  def __init__(self,
               scales=(1,3,5,7),
               aspects=(1,2)):
    """Constructs a GridKernelsizeGenerator.

    Args:
      scales: a list of (int32) scales.
      aspects: a list of (int32) kernel widths or heights

    Note: this scales sizes and aspects names are not extacly match their meaning.
    for each s_i in scales and a_j in aspects, this generator builds a set of
    kernels upon which when 2D maxpooled with stride 1, the following expected output spatial sizes are generated from the base feature map:
      - [s_i, s_i]
      - [s_i, a_j]
      - [a_j, s_i]
      These are the desired value of kernel sizes which are computed agains the
      input feature_map_shape.
    """
    self._scales = scales
    self._aspects = aspects
    if not all([isinstance(s, int) for s in scales]):
      raise ValueError('scales must be an iterator of integers.')
    if not all([isinstance(a, int) for a in aspects]):
      raise ValueError('aspects must be an iterator of integers.')

  def name_scope(self):
    return 'GridKernelsizeGenerator'

  def _generate(self, feature_map_shape_list=None):
    """Generates a collection of kernel sizes

    Args:
      feature_map_shape_list: list of (height, width) pairs in the format
        [(height_0, width_0), (height_1, width_1), ...] that the generated
        kernel sizes must align with.

    Returns:
      kernel_sizes: a list of kernel sizes
    """
    kernel_sizes = set([(s_i,s_i) for s_i in self._scales])
    kernel_sizes = kernel_sizes.union(set([(s_i,a_j) for s_i in self._scales
                                                     for a_j in self._aspects]))
    kernel_sizes = kernel_sizes.union(set([(a_j,s_i) for s_i in self._scales
                                                     for a_j in self._aspects]))
    return kernel_sizes

