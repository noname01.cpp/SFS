"""Finetuning executable for sfs faster rcnn detection models.

This executable is used to finetune SFSDetectionModels. There is one way of
configuring the finetune job.

1) A single pipeline_pb2.TrainEvalPipelineConfig file maybe specified instead.
In this mode, the --eval_training_data flag may be given to force the pipeline
to evaluate on training data instead.

Example usage:
    ./finetune \
        --logtostderr \
        --checkpoint_dir=path/to/checkpoint_dir \
        --eval_dir=path/to/eval_dir \
        --pipeline_config_path=pipeline_config.pbtxt

"""
import functools
import json
import os
import tensorflow as tf

from google.protobuf import text_format

from sfs_faster_rcnn import finetuner
from sfs_faster_rcnn import finetuner_memory
from sfs_faster_rcnn.builders import dataflow_builder
from sfs_faster_rcnn.builders import sfs_model_builder
from object_detection.protos import sfs_input_reader_pb2
from object_detection.protos import sfs_model_pb2
from object_detection.protos import sfs_pipeline_pb2
from object_detection.utils import label_map_util
from sfs_faster_rcnn.dataflow.finetune_reader import FinetuneReader

tf.logging.set_verbosity(tf.logging.INFO)

flags = tf.app.flags
#flags.DEFINE_string('checkpoint_dir', '',
#                    'Directory containing checkpoints to evaluate, typically '
#                    'set to `train_dir` used in the training job.')
flags.DEFINE_string('finetune_dir', '',
                    'Directory to write finetune training summaries to.')
flags.DEFINE_string('finetune_eval_dir', '',
                    'Directory to write finetune evaluation summaries to.')
flags.DEFINE_string('pipeline_config_path', '',
                    'Path to a pipeline_pb2.TrainEvalPipelineConfig config '
                    'file. If provided, other configs are ignored')
flags.DEFINE_string('input_config_path', '',
                    'Path to an input_reader_pb2.InputReader config file.')
flags.DEFINE_string('model_config_path', '',
                    'Path to a model_pb2.DetectionModel config file.')
flags.DEFINE_boolean('memory', False, 'use memory or hard disk for restore/save.')

FLAGS = flags.FLAGS


def get_configs_from_pipeline_file():
  """Reads evaluation configuration from a pipeline_pb2.TrainEvalPipelineConfig.

  Reads evaluation config from file specified by pipeline_config_path flag.

  Returns:
    model_config: a model_pb2.DetectionModel
    eval_config: a eval_pb2.EvalConfig
    input_config: a input_reader_pb2.InputReader
  """
  pipeline_config = sfs_pipeline_pb2.SFSTrainEvalPipelineConfig()
  with tf.gfile.GFile(FLAGS.pipeline_config_path, 'r') as f:
    text_format.Merge(f.read(), pipeline_config)

  model_config = pipeline_config.model
  #if FLAGS.eval_training_data:
  #  finetune_config = pipeline_config.train_config
  #else:
  finetune_config = pipeline_config.finetune_config
  input_config = pipeline_config.finetune_input_reader

  return model_config, finetune_config, input_config


def main(unused_argv):
#  assert FLAGS.checkpoint_dir, '`checkpoint_dir` is missing.'
  assert FLAGS.finetune_dir, '`finetune_dir` is missing.'
  assert FLAGS.finetune_eval_dir, '`finetune_eval_dir is missing.'
  assert FLAGS.pipeline_config_path, '`pipeline_config_path` is missing'
  model_config, finetune_config, input_config = get_configs_from_pipeline_file()

  #model_fn = functools.partial(
  #    sfs_model_builder.build,
  #    model_config=model_config,
  #    is_training=True)

  finetune_reader = FinetuneReader(
          input_config.query_db,
          input_config.support_db,
          input_config.split,
          n_way=model_config.sfs_faster_rcnn.num_classes,
          k_shot=model_config.sfs_faster_rcnn.k_shot,
          resize_widths=[450, 300, 224])

  print('query_db: {}'.format(input_config.query_db))
  print('support_db: {}'.format(input_config.support_db))
  print('split: {}'.format(input_config.split))
  print('n_way: {}'.format(model_config.sfs_faster_rcnn.num_classes))
  print('k_shot: {}'.format(model_config.sfs_faster_rcnn.k_shot))


  categories = [{'id': i+1, 'name': str(i+1)} for i in range(model_config.sfs_faster_rcnn.num_classes)]


  if FLAGS.memory:
    finetuner_memory.finetune(finetune_reader, model_config, finetune_config, categories,
                     FLAGS.finetune_dir, FLAGS.finetune_eval_dir)
  else:
    finetuner.finetune(finetune_reader, model_config, finetune_config, categories,
                     FLAGS.finetune_dir, FLAGS.finetune_eval_dir)


if __name__ == '__main__':
  tf.app.run()
