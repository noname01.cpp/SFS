"""A function to build an  kernel size generator from config."""

from sfs_faster_rcnn.sfs import grid_kernelsize_generator
from object_detection.protos import sfs_kernelsize_generator_pb2

def build(kernelsize_generator_config):
  """Build a kernelsize generator based on the config.
  Args:
    kernelsize_generator_config: A sfs_kernelsize_generator.proto object containing the config for the desired kernelsize generator.

  Returns:
    kernel size generator
  """
  if not isinstance(kernelsize_generator_config,
                    sfs_kernelsize_generator_pb2.SFSKernelsizeGenerator):
    raise ValueError('kernelsize_generator_config not of type '
                     'sfs_kernelsize_generator_pb2.SFSKernelsizeGenerator')
  if kernelsize_generator_config.WhichOneof(
        'kernelsize_generator_oneof') == 'sfs_grid_kernelsize_generator':
    grid_kernelsize_generator_config = kernelsize_generator_config.sfs_grid_kernelsize_generator
    return grid_kernelsize_generator.GridKernelsizeGenerator(
        scales=[int(scale) for scale in grid_kernelsize_generator_config.scales],
        aspects=[int(aspect) for aspect in grid_kernelsize_generator_config.aspects])
  else:
    raise ValueError('Empty  kernelsize generator')

