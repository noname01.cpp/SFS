from sfs_faster_rcnn.sfs import sfs_box_predictor
from object_detection.protos import sfs_box_predictor_pb2


def build(argscope_fn, sfs_box_predictor_config, is_training, num_classes):
  """Builds sfs mask rcnn box predictor based on the configuration.

  Builds box predictor based on the configuration. See box_predictor.proto for
  configurable options. Also, see box_predictor.py for more details.

  Args:
    argscope_fn: A function that takes the following inputs:
        * hyperparams_pb2.Hyperparams proto
        * a boolean indicating if the model is in training mode.
      and returns a tf slim argscope for Conv and FC hyperparameters.
    sfs_box_predictor_config: sfs_box_predictor_pb2.SFSBoxPredictor proto containing
      configuration.
    is_training: Whether the models is in training mode.
    num_classes: Number of classes to predict.

  Returns:
    box_predictor: box_predictor.BoxPredictor object.

  Raises:
    ValueError: On unknown box predictor.
  """
  if not isinstance(sfs_box_predictor_config, sfs_box_predictor_pb2.SFSBoxPredictor):
    raise ValueError('box_predictor_config not of type '
                     'sfs_box_predictor_pb2.SFSBoxPredictor.')
  sfs_box_predictor_oneof = sfs_box_predictor_config.WhichOneof('sfs_box_predictor_oneof')

  if sfs_box_predictor_oneof == 'sfs_mask_rcnn_box_predictor':
    sfs_mask_rcnn_box_predictor = sfs_box_predictor_config.sfs_mask_rcnn_box_predictor
    fc_hyperparams = argscope_fn(sfs_mask_rcnn_box_predictor.fc_hyperparams,
                                 is_training)
    conv_hyperparams = None
    if sfs_mask_rcnn_box_predictor.HasField('conv_hyperparams'):
      conv_hyperparams = argscope_fn(sfs_mask_rcnn_box_predictor.conv_hyperparams,
                                     is_training)
    box_predictor_object = sfs_box_predictor.SFSMaskRCNNBoxPredictor(
      is_training=is_training,
      num_classes=num_classes,
      fc_hyperparams=fc_hyperparams,
      use_dropout=sfs_mask_rcnn_box_predictor.use_dropout,
      dropout_keep_prob=sfs_mask_rcnn_box_predictor.dropout_keep_probability,
      box_code_size=sfs_mask_rcnn_box_predictor.box_code_size,
      conv_hyperparams=conv_hyperparams,
      predict_instance_masks=sfs_mask_rcnn_box_predictor.predict_instance_masks,
      mask_prediction_conv_depth=(sfs_mask_rcnn_box_predictor.
                                  mask_prediction_conv_depth),
      predict_keypoints=sfs_mask_rcnn_box_predictor.predict_keypoints,
      class_agnostic=sfs_mask_rcnn_box_predictor.class_agnostic)
    return box_predictor_object
  raise ValueError('Unknown box predictor: {}'.format(sfs_box_predictor_oneof))


