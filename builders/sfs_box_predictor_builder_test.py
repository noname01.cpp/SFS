import mock
import tensorflow as tf

from google.protobuf import text_format
from object_detection.builders import hyperparams_builder
from sfs_faster_rcnn.builders import sfs_box_predictor_builder
from object_detection.protos import sfs_box_predictor_pb2
from object_detection.protos import hyperparams_pb2

class SFSMaskRCNNBoxPredictorBuilderTest(tf.test.TestCase):
  def test_sfs_mask_rcnn_box_predictor_builder_calls_fc_argscope_fn(self):
    fc_hyperparams_text_proto = """
      regularizer {
        l1_regularizer {
          weight: 0.0003
        }
      }
      initializer {
        truncated_normal_initializer {
          mean: 0.0
          stddev: 0.3
        }
      }
      activation: RELU_6
      op: FC
    """
    hyperparams_proto = hyperparams_pb2.Hyperparams()
    text_format.Merge(fc_hyperparams_text_proto, hyperparams_proto)
    box_predictor_proto = sfs_box_predictor_pb2.SFSBoxPredictor()
    box_predictor_proto.sfs_mask_rcnn_box_predictor.fc_hyperparams.CopyFrom(
        hyperparams_proto)
    mock_argscope_fn = mock.Mock(return_value='arg_scope')
    box_predictor = sfs_box_predictor_builder.build(
        argscope_fn=mock_argscope_fn,
        sfs_box_predictor_config=box_predictor_proto,
        is_training=False,
        num_classes=10)
    mock_argscope_fn.assert_called_with(hyperparams_proto, False)
    self.assertEqual(box_predictor._fc_hyperparams, 'arg_scope')

  def test_non_default_sfs_mask_rcnn_box_predictor(self):
    fc_hyperparams_text_proto = """
      regularizer {
        l1_regularizer {
        }
      }
      initializer {
        truncated_normal_initializer {
        }
      }
      activation: RELU_6
      op: FC
    """
    box_predictor_text_proto = """
      sfs_mask_rcnn_box_predictor {
        use_dropout: true
        dropout_keep_probability: 0.8
        box_code_size: 3
        class_agnostic: true
      }
    """
    hyperparams_proto = hyperparams_pb2.Hyperparams()
    text_format.Merge(fc_hyperparams_text_proto, hyperparams_proto)
    def mock_fc_argscope_builder(fc_hyperparams_arg, is_training):
      return (fc_hyperparams_arg, is_training)

    box_predictor_proto = sfs_box_predictor_pb2.SFSBoxPredictor()
    text_format.Merge(box_predictor_text_proto, box_predictor_proto)
    box_predictor_proto.sfs_mask_rcnn_box_predictor.fc_hyperparams.CopyFrom(
        hyperparams_proto)
    box_predictor = sfs_box_predictor_builder.build(
        argscope_fn=mock_fc_argscope_builder,
        sfs_box_predictor_config=box_predictor_proto,
        is_training=True,
        num_classes=90)
    self.assertTrue(box_predictor._use_dropout)
    self.assertAlmostEqual(box_predictor._dropout_keep_prob, 0.8)
    self.assertEqual(box_predictor.num_classes, 90)
    self.assertTrue(box_predictor._is_training)
    self.assertEqual(box_predictor._box_code_size, 3)
    self.assertTrue(box_predictor._class_agnostic)

  def test_build_default_sfs_mask_rcnn_box_predictor(self):
    box_predictor_proto = sfs_box_predictor_pb2.SFSBoxPredictor()
    box_predictor_proto.sfs_mask_rcnn_box_predictor.fc_hyperparams.op = (
        hyperparams_pb2.Hyperparams.FC)
    box_predictor = sfs_box_predictor_builder.build(
        argscope_fn=mock.Mock(return_value='arg_scope'),
        sfs_box_predictor_config=box_predictor_proto,
        is_training=True,
        num_classes=90)
    self.assertFalse(box_predictor._use_dropout)
    self.assertAlmostEqual(box_predictor._dropout_keep_prob, 0.5)
    self.assertEqual(box_predictor.num_classes, 90)
    self.assertTrue(box_predictor._is_training)
    self.assertEqual(box_predictor._box_code_size, 4)
    self.assertFalse(box_predictor._predict_instance_masks)
    self.assertFalse(box_predictor._predict_keypoints)
    self.assertFalse(box_predictor._class_agnostic)

  #def test_build_sfs_box_predictor_with_mask_branch(self):
    #box_predictor_proto = sfs_box_predictor_pb2.SFSBoxPredictor()
    #box_predictor_proto.sfs_mask_rcnn_box_predictor.fc_hyperparams.op = (
        #hyperparams_pb2.Hyperparams.FC)
    #box_predictor_proto.sfs_mask_rcnn_box_predictor.conv_hyperparams.op = (
        #hyperparams_pb2.Hyperparams.CONV)
    #box_predictor_proto.sfs_mask_rcnn_box_predictor.predict_instance_masks = True
    #box_predictor_proto.sfs_mask_rcnn_box_predictor.mask_prediction_conv_depth = 512
    #mock_argscope_fn = mock.Mock(return_value='arg_scope')
    #box_predictor = sfs_box_predictor_builder.build(
        #argscope_fn=mock_argscope_fn,
        #sfs_box_predictor_config=box_predictor_proto,
        #is_training=True,
        #num_classes=90)
    #mock_argscope_fn.assert_has_calls(
        #[mock.call(box_predictor_proto.sfs_mask_rcnn_box_predictor.fc_hyperparams,
                   #True),
         #mock.call(box_predictor_proto.sfs_mask_rcnn_box_predictor.conv_hyperparams,
                   #True)], any_order=True)
    #self.assertFalse(box_predictor._use_dropout)
    #self.assertAlmostEqual(box_predictor._dropout_keep_prob, 0.5)
    #self.assertEqual(box_predictor.num_classes, 90)
    #self.assertTrue(box_predictor._is_training)
    #self.assertEqual(box_predictor._box_code_size, 4)
    #self.assertTrue(box_predictor._predict_instance_masks)
    #self.assertEqual(box_predictor._mask_prediction_conv_depth, 512)
    #self.assertFalse(box_predictor._predict_keypoints)
    #self.assertFalse(box_predictor._class_agnostic)

if __name__ == '__main__':
  tf.test.main()
