from sfs_faster_rcnn.dataflow.tensorpack_utils import TensorpackReader, TensorpackReaderNoSS, augment_query_images, augment_supportset
from sfs_faster_rcnn.sfs import standard_fields as fields
from object_detection.protos import sfs_input_reader_pb2
from tensorpack import *
import tensorflow as tf

def build(dataflow_config,
    queue_size,
    is_training,
    has_supportset=True,
    n_way=3,
    k_shot=5):
  if not isinstance(dataflow_config, sfs_input_reader_pb2.SFSInputReader):
    raise ValueError('dataflow_config not of type '
      'sfs_input_reader_pb2.SFSInputReader.')
  if has_supportset:
    ds = TensorpackReader(dataflow_config.query_db, dataflow_config.support_db,
                          split=dataflow_config.split, n_way=n_way, k_shot=k_shot,
                          selection_prob=dataflow_config.keep_probability)
  else:
    ds = TensorpackReaderNoSS(dataflow_config.query_db,
      split=dataflow_config.split)

  ds = augment_query_images(ds, is_training, has_supportset)
  if has_supportset:
    ds = augment_supportset(ds, is_training, k_shot, n_way)

  ds.reset_state()

  if is_training:
    ds = PrefetchDataZMQ(ds, dataflow_config.num_readers)
  else:
    ds = PrefetchDataZMQ(ds, 1)

  if has_supportset:
    placeholders = [tf.placeholder(tf.float32, shape=(None, None, 3)), # query image
      tf.placeholder(tf.float32, shape=(None, 4)), # query boxes
      tf.placeholder(tf.float32, shape=(None,)), # query classes
      tf.placeholder(tf.float32, shape=(None,)), # query original classes
      tf.placeholder(tf.float32, shape=(n_way*k_shot, 300, 300, 3)), #supportset images
      tf.placeholder(tf.float32, shape=(n_way,k_shot,4)), #supportset boxes computed per n_way
      tf.placeholder(tf.int32,   shape=(n_way,k_shot))] #box2image indices
    #placeholders += [tf.placeholder(tf.float32, shape=(None,4)) for _ in range(n_way*k_shot)] #supportset boxes
  else:
    placeholders = [tf.placeholder(tf.float32, shape=(None, None, 3)), # query image
      tf.placeholder(tf.float32, shape=(None, 4)), # query boxes
      tf.placeholder(tf.float32, shape=(None,))] # query classes

  queue = tf.FIFOQueue(queue_size, [x.dtype for x in placeholders])
  thread = graph_builder.EnqueueThread(queue, ds, placeholders)
  tensors = queue.dequeue()
  for tensor, placeholder in zip(tensors, placeholders):
    tensor.set_shape(placeholder.shape)
  if has_supportset:
    tensors_dict = {fields.SFSInputDataFields.query: tensors[0],
                 fields.SFSInputDataFields.groundtruth_boxes: tensors[1],
                 fields.SFSInputDataFields.groundtruth_classes: tensors[2],
                 fields.SFSInputDataFields.groundtruth_orig_classes: tensors[3],
                 fields.SFSInputDataFields.supportset: tensors[4],
                 fields.SFSInputDataFields.supportset_boxes: tensors[5],
                 fields.SFSInputDataFields.support_box2img_indices: tensors[6]}
    #for n in range(n_way):
    #  for k in range(k_shot):
    #    tensors_dict[fields.SFSInputDataFields.supportset_boxes+'_{}_{}'.format(n,k)] = tensors[4+n*k_shot+k]
  else:
    tensors_dict = {fields.SFSInputDataFields.query: tensors[0],
                 fields.SFSInputDataFields.groundtruth_boxes: tensors[1],
                 fields.SFSInputDataFields.groundtruth_classes: tensors[2]}

  return tensors_dict, thread
