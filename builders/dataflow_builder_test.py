from sfs_faster_rcnn.sfs import standard_fields as fields
from google.protobuf import text_format
from sfs_faster_rcnn.builders import dataflow_builder
from object_detection.protos import sfs_input_reader_pb2
import os
import os.path as osp
import shutil
import cv2
import tensorflow as tf
import numpy as np

def build_dataflow(text_proto, has_supportset=True, n_way=3, k_shot=5):
  dataflow_config = sfs_input_reader_pb2.SFSInputReader()
  text_format.Merge(text_proto, dataflow_config)
  return dataflow_builder.build(
    dataflow_config,
    queue_size=50,
    is_training=True,
    n_way=n_way,
    k_shot=k_shot,
    has_supportset=has_supportset)

def draw_anns(img, boxes, tags, image_tag=None):
  ps = 50
  h, w = img.shape[:2]
  img = np.pad(img, ((ps,ps),(ps,ps),(0,0)), 'constant')
  for box, tag in zip(boxes, tags):
    color = (0,255,0)
    y1 = int(box[0]*h + ps)
    x1 = int(box[1]*w + ps)
    y2 = int(box[2]*h + ps)
    x2 = int(box[3]*w + ps)
    #x1 = int(box[0] + ps)
    #y1 = int(box[1] + ps)
    #x2 = int(box[2] + ps)
    #y2 = int(box[3] + ps)
    cv2.putText(img, str(tag), tuple((x1+10,y1+10)), cv2.FONT_HERSHEY_SIMPLEX, .4, color)
    cv2.rectangle(img, (x1,y1), (x2,y2), (0,0,255), 2)
  if image_tag is not None:
    color = (0,255,0)
    cv2.putText(img, image_tag, (10,10), cv2.FONT_HERSHEY_SIMPLEX, .5, color, 2)
  return img

def viz(path, query_img, boxes, classes, supportset, supportset_boxes, k_shot,
        support_box2img_indices, n_way):
  query_img = query_img[...,::-1]
  query_img = draw_anns(query_img, boxes, classes)
  query_path = os.path.join(path, 'query.jpg')
  cv2.imwrite(query_path, query_img)
  supportset = supportset[...,::-1]
  for n in range(n_way):
    for k in range(k_shot):
      box = supportset_boxes[n,k].reshape((1,4))
      if np.any(box > 1):
        continue
      support_img = supportset[support_box2img_indices[n,k]]
      support_img_s = draw_anns(support_img, box, np.ones((1,))*n)
      support_img_path = os.path.join(path, 'support_box_{}_{}.jpg'.format(n,k))
      cv2.imwrite(support_img_path, support_img_s)
  #for support_img,support_boxes in zip(supportset, supportset_boxes):
  #  support_img_s = draw_anns(support_img, support_boxes, np.ones((support_boxes.shape[0],))*(i/k_shot))
  #  support_img_path = os.path.join(path, 'support_{}_{}.jpg'.format(i/k_shot,i%k_shot))
  #  cv2.imwrite(support_img_path, support_img_s)
  #  i = i + 1

if __name__ == '__main__':
  dataflow_proto = '''
    shuffle: true
    num_readers: 8
    split: "os_val"
    query_db: "/home/amir/detection-data/det/ILSVRC2014_processed/"
    support_db: "/home/amir/detection-data/cls/boxed_imagenet_processed/"
  '''
  n_way=3
  k_shot=4
  tensors_dict, thread = build_dataflow(dataflow_proto, has_supportset=True, n_way=n_way, k_shot=k_shot)

  viz_path = './viz2'
  if osp.exists(viz_path):
    shutil.rmtree(viz_path)

  os.mkdir(viz_path)
  iters = 1000

  with tf.Session() as sess:
    with sess.as_default():
      thread.start()
    for i in range(iters):
      arrs_dict = sess.run(tensors_dict)
      query_img = arrs_dict[fields.SFSInputDataFields.query]
      boxes = arrs_dict[fields.SFSInputDataFields.groundtruth_boxes]
      supportset = arrs_dict[fields.SFSInputDataFields.supportset]
      supportset_boxes = arrs_dict[fields.SFSInputDataFields.supportset_boxes] #n_way,k_shot,4
      support_box2img_indices = arrs_dict[fields.SFSInputDataFields.support_box2img_indices] #n_way,k_shot here
      #for n in range(n_way):
      #  for k in range(k_shot):
      #    supportset_boxes.append(arrs_dict[fields.SFSInputDataFields.supportset_boxes+'_{}_{}'.format(n,k)])

      classes = np.array(
          arrs_dict[fields.SFSInputDataFields.groundtruth_classes],
          dtype=np.int32)
      orig_classes = np.array(
          arrs_dict[fields.SFSInputDataFields.groundtruth_orig_classes],
          dtype=np.int32)
      if i % 100 == 0:
        print('Step #%d/%d' % (i, iters))
        npath = osp.join(viz_path, 'index_' + str(i))
        os.mkdir(npath)
        viz(npath, query_img,
            boxes, orig_classes,
            supportset,
            supportset_boxes,
            k_shot,
            support_box2img_indices,
            n_way)
