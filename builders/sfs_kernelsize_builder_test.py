
from google.protobuf import text_format
from sfs_faster_rcnn.builders import sfs_kernelsize_builder
from object_detection.protos import sfs_kernelsize_generator_pb2

def build_sfs_kernelsize_builder(text_proto):
  mx_kns_gen_config = sfs_kernelsize_generator_pb2.SFSKernelsizeGenerator()
  text_format.Merge(text_proto, mx_kns_gen_config)
  return sfs_kernelsize_builder.build(mx_kns_gen_config)


if __name__ == '__main__':
  text_proto = """
    sfs_grid_kernelsize_generator {
      scales: [1,3,5,7]
      aspects: [1,2]
    }
  """
  kernelsize_generator = build_sfs_kernelsize_builder(text_proto)
  print kernelsize_generator.generate([(7,7)])
