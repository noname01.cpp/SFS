"""SFS Detection model evaluator.

This file provides a generic evaluation method that can be used to evaluate a
DetectionModel.
"""

import functools
import logging
import tensorflow as tf

from sfs_faster_rcnn.builders import sfs_model_builder

from sfs_faster_rcnn import eval_util
from object_detection.core import box_list
from object_detection.core import box_list_ops
from object_detection.core import prefetcher
from sfs_faster_rcnn.sfs import standard_fields as fields
from object_detection.utils import ops
from object_detection.utils import variables_helper
from object_detection.builders import optimizer_builder
from IPython import embed
from datetime import datetime
import numpy as np
import os, sys
from dataflow.finetune_reader_test import viz

slim = tf.contrib.slim

EVAL_METRICS_FN_DICT = {
    'pascal_voc_metrics': eval_util.evaluate_detection_results_pascal_voc
}

def evaluate_class_queries(finetune_reader, model_config, finetune_config, finetune_dir, global_step, train_num, counters, categories, finetune_eval_dir, visualize, reset=True):
  """
  """
  with tf.Graph().as_default():
    create_model_fn = functools.partial(
        sfs_model_builder.build,
        model_config=model_config,
        is_training=False)

    # create model
    model = create_model_fn()

    # configproto of tf
    configproto = tf.ConfigProto()
    configproto.gpu_options.allow_growth = True
    #configproto.allow_soft_placement = True

    # logging
    tf.logging.set_verbosity(tf.logging.INFO)

    # create placeholders -> image, box, label
    label_id_offset = 1
    input_image  = tf.placeholder(tf.float32, shape=(None, None, 3), name='input_image')
    location_gt  = tf.placeholder(tf.float32, shape=(None,4), name='location_gt')
    classes_input  = tf.placeholder(tf.float32, shape=(None,), name='classes_input')
    classes_gt = tf.cast(classes_input, tf.int32)
    classes_gt -= label_id_offset
    classes_gt = ops.padded_one_hot_encoding(indices=classes_gt,
                                                  depth=model._num_classes,
                                                  left_pad=0)

    # feed image, ground truths, get predictions
    original_image = tf.expand_dims(input_image, 0)

    preprocessed_image = model.preprocess(tf.to_float(original_image), is_query=True)
    model.provide_groundtruth([location_gt], [classes_gt], None)
    prediction_dict = model.predict(preprocessed_image, None)

    ###
    #postprocess for query image,
    detections = model.postprocess(prediction_dict)
    original_image_shape = tf.shape(original_image)
    absolute_detection_boxlist = box_list_ops.to_absolute_coordinates(
        box_list.BoxList(tf.squeeze(detections['detection_boxes'], axis=0)),
        original_image_shape[1], original_image_shape[2])
    classes = detections.get('detection_classes')
    if classes != None:
      classes = classes + label_id_offset
    else:
      classes = tf.constant(1, tf.int32, detections['detection_scores'].shape)

    tensor_dict = {
        'original_image': original_image,
        'detection_boxes': absolute_detection_boxlist.get(),
        'detection_scores': tf.squeeze(detections['detection_scores'], axis=0),
        'detection_classes': (tf.squeeze(classes, axis=0) ),
    }

    # load groundtruth fields into tensor_dict
    normalized_gt_boxlist = box_list.BoxList(
          location_gt)
    gt_boxlist = box_list_ops.scale(normalized_gt_boxlist,
                                    tf.shape(original_image)[1],
                                    tf.shape(original_image)[2])
    groundtruth_boxes = gt_boxlist.get()
    groundtruth_classes = classes_input
    tensor_dict['groundtruth_boxes'] = groundtruth_boxes
    tensor_dict['groundtruth_classes'] = groundtruth_classes
    ### end post process of the query image
    saver = tf.train.Saver()
    # create session
    with tf.Session(config=configproto) as sess:
      #initialize
      checkpoint_file = os.path.join(finetune_dir, 'checkpoints', 'finetune_model_{}.ckpt-{}'.format(global_step, global_step))
      saver.restore(sess, checkpoint_file)


      original_classes = finetune_reader.ds.sampler.classes_to_choose

      all_images = [finetune_reader.get_num_query_images(original_class) for original_class in original_classes]

      num_examples = min(finetune_config.num_examples, np.prod(all_images))

      print('Evaluating for {} examples of original classes {}'.format(num_examples, original_classes))
      result_lists = None
      if reset == True: # simple reset of random state
        finetune_reader.reset_query_data(classes_seeds=7, selector_seed=13)
        finetune_reader.change_query_data()
      for example in xrange(num_examples):
        # test on query image
        query_image, query_boxes, query_classes, query_orig_classes = finetune_reader.query_data()
        if result_lists is None:
          result_lists = {key: [] for key in tensor_dict.keys()}

        ###TODO decide about how to do visualizations
        if visualize == False and train_num >= finetune_config.num_visualizations:
          if 'original_image' in tensor_dict:
            tensor_dict = {k: v for (k,v) in tensor_dict.items()
                           if k!= 'original_image'}

        try:
          result_dict = sess.run(tensor_dict, feed_dict={input_image: query_image,
                                                              location_gt: query_boxes,
                                                              classes_input: query_classes})
          counters['success'] += 1
        except tf.errors.InvalidArgumentError:
          logging.info('Skipping image')
          counters['skipped'] += 1
          return result_lists

        ### TODO: decide about visualizations
        if visualize == True and example < finetune_config.num_visualizations:
          if not os.path.exists(os.path.join(finetune_dir,'export')):
            os.makedirs(os.path.join(finetune_dir, 'export'))
          for thresh in [0.1, 0.3, 0.5]:
            tag = '{}-query-{}-{}-{}'.format(train_num, example, global_step, thresh)
            eval_util.visualize_detection_results(
              result_dict, tag, global_step, categories=categories,
              summary_dir=finetune_eval_dir,
              export_dir=os.path.join(finetune_dir,'export'),
              show_groundtruth=True,
              min_score_thresh=thresh,
              add_summary=False)
        for key in result_dict:
          result_lists[key].append(result_dict[key])

        finetune_reader.change_query_data()
    return result_lists

def finetune_mini_dataset(finetune_reader, model_config, finetune_config, finetune_dir, train_num,
                          print_every_step=10, summary_every_train_num=100, save_every_step=200):
  """
  """
  with tf.Graph().as_default():
    global_step = tf.Variable(0, name='global_step', trainable=False)
    create_model_fn = functools.partial(
        sfs_model_builder.build,
        model_config=model_config,
        is_training=True)
    # create model
    model = create_model_fn()

    # initial summaries
    summaries = set(tf.get_collection(tf.GraphKeys.SUMMARIES))
    global_summaries = set([])

    # configproto of tf
    configproto = tf.ConfigProto()
    configproto.gpu_options.allow_growth = True
    #configproto.allow_soft_placement = True

    # logging
    tf.logging.set_verbosity(tf.logging.INFO)

    # create placeholders -> image, box, label
    label_id_offset = 1
    input_image  = tf.placeholder(tf.float32, shape=(None, None, 3), name='input_image')
    location_gt  = tf.placeholder(tf.float32, shape=(None,4), name='location_gt')
    classes_input  = tf.placeholder(tf.float32, shape=(None,), name='classes_input')
    classes_gt = tf.cast(classes_input, tf.int32)
    classes_gt -= label_id_offset
    classes_gt = ops.padded_one_hot_encoding(indices=classes_gt,
                                                  depth=model._num_classes,
                                                  left_pad=0)

    # feed image, ground truths, get predictions
    original_image = tf.expand_dims(input_image, 0)

    preprocessed_image = model.preprocess(tf.to_float(original_image), is_query=True)
    model.provide_groundtruth([location_gt], [classes_gt], None)
    prediction_dict = model.predict(preprocessed_image, None)

    # get loss
    losses_dict = model.loss(prediction_dict)
    print('Losses: {}'.format(losses_dict))
    for loss_tensor in losses_dict.values():
      tf.losses.add_loss(loss_tensor)

    # Gather update_ops, batch_norm vars, etc created by model TODO: no scope vs 1st clone
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    print('Update ops length {}'.format(len(update_ops)))

    # create optimizer
    training_optimizer = optimizer_builder.build(finetune_config.optimizer, global_summaries)

    # restore
    init_fn = None
    if finetune_config.fine_tune_checkpoint:
      var_map = model.restore_map(
          from_detection_checkpoint=finetune_config.from_detection_checkpoint,
          finetuning=True,
          restore_box_encoder=finetune_config.restore_box_encoding_predictor)
      #print('**** VAR MAP ****')
      #for key in var_map.keys():
      #  print('{}'.format(key))
      #print('****  END  *****')
      available_var_map = (variables_helper.
                            get_variables_available_in_checkpoint(
                              var_map, finetune_config.fine_tune_checkpoint))
      init_saver = tf.train.Saver(available_var_map)
      def initializer_fn(sess):
        init_saver.restore(sess, finetune_config.fine_tune_checkpoint)
      init_fn = initializer_fn

    # find var_list for gradients
    if finetune_config.restore_box_encoding_predictor and finetune_config.fix_box_encoding_predictor:
      box_predictor_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=model.second_stage_box_predictor_scope+'/'+model._mask_rcnn_box_predictor.class_predictor_scope)
    else:
      box_predictor_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=model.second_stage_box_predictor_scope)

    for loss_name, loss_value in losses_dict.iteritems():
      tf.summary.scalar(loss_name, loss_value)

    ### DEBUG
    #trainable_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    #print('trainable vars size {}'.format(len(trainable_vars)))
    print('second stage box predictor vars size {}'.format(len(box_predictor_vars)))
    print(box_predictor_vars)
    #for i, var in enumerate(trainable_vars):
    #  print('[{}] -- {}'.format(i, var))
    ### END

    # take gradients and updates
    with tf.name_scope('optimization'):
      output_loss = tf.add_n(losses_dict.values())
      tf.summary.scalar('output_loss', output_loss)

      #TODO regularization losses are empty lists

      ### DEBUG
      #regu_losses = tf.losses.get_regularization_losses()
      #print('Regularization losses {}'.format(regu_losses))
      #regu_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
      #print('Regularization losses {}'.format(regu_losses))
      #regularization_loss = tf.add_n(tf.losses.get_regularization_losses())
      #tf.summary.scalar('regularization_loss', regularization_loss)

      total_loss = output_loss # + regularization_loss
      tf.summary.scalar('total_loss', total_loss)

      grads_and_vars = training_optimizer.compute_gradients(total_loss, var_list=box_predictor_vars)
      if finetune_config.gradient_clipping_by_norm > 0:
        with tf.name_scope('clip_grads'):
          grads_and_vars = slim.learning.clip_gradient_norms(
                              grads_and_vars, finetune_config.gradient_clipping_by_norm)
      with tf.name_scope('take_gradients'):
        apply_gradient_op = training_optimizer.apply_gradients(grads_and_vars, global_step=global_step)

    ### Experimental
    #update_ops.append(apply_gradient_op)
    #update_op = tf.group(*update_ops)
    #with tf.control_dependencies([update_op]):
    #  train_tensor = tf.identity(total_loss, name='train_op')

    #for model_var in slim.get_model_variables():
    #  global_summaries.add(tf.summary.histogram(model_var.op.name, model_var))

    #for loss_tensor in tf.losses.get_losses():
    #  global_summaries.add(tf.summary.scalar(loss_tensor.op.name, loss_tensor))

    #global_summaries.add(
    #    tf.summary.scalar('TotalLoss', tf.losses.get_total_loss()))

    #summaries |= set(tf.get_collection(tf.GraphKeys.SUMMARIES))
    #summaries |= global_summaries
    ### END

    # merge summaries
    merged_summary_op = tf.summary.merge_all()
    #merged_summary_op = tf.summary.merge(list(summaries), name='summary_op')

    # init global vars
    init = tf.global_variables_initializer()
    saved_steps = []

    # create session
    with tf.Session(config=configproto) as sess:
      #initialize
      sess.run(init) #TODO: I am not sure but this works!
      sess.run(tf.local_variables_initializer())
      init_fn(sess)
      print('Weights initialized')

      # create a Saver for managing checkpoints
      saver = tf.train.Saver(max_to_keep=None)

      # create summary writer
      if train_num % summary_every_train_num == 0:
        summary_writer = tf.summary.FileWriter(finetune_dir+'/'+str(train_num), graph=tf.get_default_graph())
      step = 1
      while step < finetune_config.num_steps + 1:
        image, boxes, labels = finetune_reader.support_data()

        #do training on support set
        if train_num % summary_every_train_num == 0:
          run_res = sess.run([total_loss, merged_summary_op, apply_gradient_op,
                              model._debug_batch_cls_targets,
                              model._debug_batch_cls_weights,
                              model._debug_num_proposals], #TODO grad_accumulator_ops ?
                          feed_dict={input_image: image, location_gt: boxes, classes_input: labels})
          batch_loss  = run_res[0]
          summary     = run_res[1]
          cls_targets = run_res[3]
          cls_weights = run_res[4]
          num_proposals = run_res[5]
        else:
          batch_loss,_,cls_targets, cls_weights, num_proposals = sess.run([total_loss, apply_gradient_op, model._debug_batch_cls_targets, model._debug_batch_cls_weights, model._debug_num_proposals],
                                feed_dict={input_image: image, location_gt: boxes, classes_input: labels})

        #from IPython import embed; embed()
        #print('CLS_TARGETS SUM: {}'.format(np.sum(cls_targets, axis=1)))
        #print('NUM_PROPOSALS:   {}'.format(num_proposals))

        if train_num % summary_every_train_num == 0:
          summary_writer.add_summary(summary, step)
        if step % print_every_step == 0:
          print >> sys.stderr, "{} Iter {}: Training {} Loss = {:.4f}".format(datetime.now(), step, train_num, batch_loss)

        if step % save_every_step == 0:
          print >> sys.stderr, "Saving model Iter {}".format(step)
          if not os.path.exists(os.path.join(finetune_dir,'checkpoints')):
            os.makedirs(os.path.join(finetune_dir, 'checkpoints'))

          model_name = os.path.join(finetune_dir, 'checkpoints', 'finetune_model_{}.ckpt'.format(step))
          saver.save(sess, model_name, global_step=step)
          saved_steps.append(step)

        step += 1
      print('Finetuning #{} Finished'.format(train_num))
    return saved_steps

def finetune(finetune_reader, model_config, finetune_config, categories,
             finetune_dir, finetune_eval_dir):
  """Evaluation function for detection models.

  Args:
    finetune_reader: a reader for sampling mini-datasets.
    model_config: a config of a DetectionModel.
    finetune_config: a sfs_finetune_pb2.SFSFinetuneConfig protobuf.
    finetune_dir: directory to write training summaries.
    finetune_eval_dir: directory to write evaluation metrics summary to.
  """

  def _process_aggregated_results(result_lists, first_stage_only, categories):
    if not first_stage_only:
      eval_metric_fn_key = finetune_config.metrics_set
      if eval_metric_fn_key not in EVAL_METRICS_FN_DICT:
        raise ValueError('Metric not found: {}'.format(eval_metric_fn_key))
      return EVAL_METRICS_FN_DICT[eval_metric_fn_key](result_lists,
                                                    categories=categories,
                                                    iou_thres=0.5)
    else:
      return eval_util.evaluate_rpn_results(result_lists, [0.5,0.6,0.7,0.8,0.9])

  counters = {'skipped': 0, 'success': 0}
  classes_metrics = []
  num_classes = min(finetune_reader.get_num_classes(), finetune_config.num_class_samples)
  saved_steps = None
  num_classes_to_viz = 10
  num_support_per_class_to_viz = 3

  for i in xrange(num_classes):
    print('Finetuning for the {} class'.format(i))
    finetune_reader.next_classes()
    finetune_reader.change_support_data()
    finetune_reader.change_query_data()
    this_class_metrics = None
    for j in xrange(finetune_config.num_supportset_samples):
      train_num = j + i * finetune_config.num_supportset_samples
      saved_steps = finetune_mini_dataset(finetune_reader,
                                          model_config,
                                          finetune_config,
                                          finetune_dir,
                                          train_num,
                                          summary_every_train_num=30,
                                          save_every_step=100)
      if this_class_metrics == None:
        this_class_metrics = [ [] for _ in saved_steps]
      visualize = False
      if i < num_classes_to_viz and j < num_support_per_class_to_viz:
        visualize = True
      for k, global_step in enumerate(saved_steps):
        result_lists = evaluate_class_queries(finetune_reader,
                                              model_config,
                                              finetune_config,
                                              finetune_dir,
                                              global_step,
                                              train_num, counters,
                                              categories,
                                              finetune_eval_dir,
                                              visualize)
        print('='*40)
        print('Evaluation Results for step {} of {} results of class {}, and support sample {}'.format(global_step, len(result_lists['detection_boxes']), i, j))
        with tf.Session() as sess:
          (metrics, hists) = _process_aggregated_results(result_lists, False, categories)
          for key in sorted(metrics):
            print('{} -- {}: {} for train num {}'.format(global_step, key, metrics[key], train_num))

          this_class_metrics[k].append(metrics)
          #eval_util.write_metrics(metrics, step, finetune_eval_dir, hists)
        print('='*40)

      if visualize == True:
        if not os.path.exists(os.path.join(finetune_dir,'export')):
            os.makedirs(os.path.join(finetune_dir, 'export'))
        imgs, boxes, labels = finetune_reader.original_support_data()
        for s_i, (img, box, label) in enumerate(zip(imgs, boxes, labels)):
          name = '{}-suport-{}.png'.format(train_num, s_i)
          print('Saving image {}'.format(name))
          path = os.path.join(finetune_dir, 'export',  name)
          viz(path, img, box, label)


      finetune_reader.change_support_data()
    classes_metrics.append(this_class_metrics)

  total_mean_step = [0.0 for step in saved_steps]
  total_std_step  = [0.0 for step in saved_steps]
  for cls, cls_metrics in enumerate(classes_metrics):
    for i, (step, metrics) in enumerate(zip(saved_steps, cls_metrics)):
      cls_step_mean = np.mean([metric['Precision/mAP@0.5IOU'] for metric in metrics])
      cls_step_std  = np.std([metric['Precision/mAP@0.5IOU'] for metric in metrics])
      print('--- step {}: mAP@0.5IoU for class #{}: {} +- {}'.format(step, cls, cls_step_mean, cls_step_std))
      total_mean_step[i] += cls_step_mean
      total_std_step[i] += cls_step_std

  total_mean_step = np.array(total_mean_step, dtype=np.float32)/len(classes_metrics)
  total_std_step  = np.array(total_std_step, dtype=np.float32)/len(classes_metrics)
  for i, (step, total_mean) in enumerate(zip(saved_steps, total_mean_step)):
    print('Step {}: Total mAP@0.5IoU is {} +- {}'.format(step, total_mean, total_std_step[i]))



  #print('# success: %d' % counters['success'])
  #print('# skipped: %d' % counters['skipped'])

  #for result_lists, step in zip(result_lists_list, saved_steps):
  #  print('='*20)
  #  print('Evaluation Results for step {}'.format(step))
  #  with tf.Session() as sess:
  #    (metrics, hists) = _process_aggregated_results(result_lists, False)
  #    eval_util.write_metrics(metrics, step, finetune_eval_dir, hists)
  #  print('='*20)


