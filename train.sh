#rm -r train
CUDA_VISIBLE_DEVICES="1" python train.py --logtostderr --pipeline_config_path=./configs/sfs_faster_rcnn_resnet50_imagenet_140.config --train_dir=./logs/train_agnostic_long/ --num_clones=1
