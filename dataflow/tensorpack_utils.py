from data_sampler import DataSampler
from data_sampler_no_supportset import DataSamplerNoSS
from tensorpack import *
from tensorpack.dataflow.imgaug.transform import CropTransform, ResizeTransform
import os.path as osp
import numpy as np
from skimage.transform import resize
import copy
import cv2
import sys, traceback

# supportset: n_way*k_shot list of cls_height,cls_width,3
# query_img: h,w,3
# query_rois: n_boxes, 4
# query_classes: n_boxes,
# supportset_boxes: n_way*k_shot list of m_boxes,4
# supportset_classes: n_way*k_shot list of m_boxes

#TODO: add rng
class TensorpackReader(RNGDataFlow):
  def __init__(self, det_root, cls_root, split,
              n_way=1, k_shot=1, selection_prob=1.):
    self.det_root = det_root
    self.cls_root = cls_root
    self.n_way = n_way
    self.k_shot = k_shot
    self.split = split
    self.sampler = DataSampler(det_root, cls_root, split, n_way, k_shot, selection_prob)

  def size(self):
    return self.sampler.det_reader.nr_images

  def _load_supportset_images(self, sample):
    imgs = []
    for way in xrange(self.n_way):
      for k in xrange(self.k_shot):
        img_path = osp.join(self.cls_root, 'Images', sample['supportset_imgs'][way][k])
        img = cv2.imread(img_path, cv2.IMREAD_COLOR)
        imgs.append(img)
    return imgs

  def _load_det_img(self, sample):
    img_path = osp.join(self.det_root, 'Images', sample['query_img'])
    img = cv2.imread(img_path, cv2.IMREAD_COLOR)
    return img

  def _unpack_support_roi_boxes(self, sample):
    supportset_boxes = []
    for way in xrange(self.n_way):
      for k in xrange(self.k_shot):
        supportset_boxes.append(sample['supportset_rois'][way][k]['boxes'])
    return supportset_boxes

  def get_data(self):
    for _ in list(xrange(self.size())):
      sample = self.sampler.next()
      supportset_imgs = self._load_supportset_images(sample)
      query_img = self._load_det_img(sample)
      query_boxes = sample['query_rois']['boxes']
      query_classes = sample['query_rois']['classes']
      query_orig_classes = sample['query_rois']['orig_classes']
      supportset_boxes = self._unpack_support_roi_boxes(sample)
      yield [query_img, query_boxes, query_classes, query_orig_classes, supportset_imgs, supportset_boxes]


# query_img: h, w, 3
# query_rois: n_boxes, 4 -> list of numpy arrays
# query_classes: n_boxes

class TensorpackReaderNoSS(RNGDataFlow):
  def __init__(self, det_root, split):
    self.det_root = det_root
    self.split = split
    self.sampler = DataSamplerNoSS(det_root, split)

  def size(self):
    return self.sampler.det_reader.nr_images

  def _load_det_img(self, sample):
    img_path = osp.join(self.det_root, 'Images', sample['query_img'])
    img = cv2.imread(img_path, cv2.IMREAD_COLOR)
    return img

  def get_data(self):
    for _ in list(xrange(self.size())):
      sample = self.sampler.next()
      query_img = self._load_det_img(sample)
      query_boxes = sample['query_rois']['boxes']
      query_classes = sample['query_rois']['classes']
      yield [query_img, query_boxes, query_classes]

class GoogleNetResize(imgaug.ImageAugmentor):
  """
  crop 8%~100% of the original image
  See `Going Deeper with Convolutions` by Google.
  """
  def __init__(self, crop_area_fraction=0.08,
              aspect_ratio_low=0.75, aspect_ratio_high=1.333):
    self._init(locals())

  def _get_augment_params(self, img):
    h, w = img.shape[:2]
    area = h * w
    for _ in range(10):
      targetArea = self.rng.uniform(self.crop_area_fraction, 1.0) * area
      aspectR = self.rng.uniform(self.aspect_ratio_low, self.aspect_ratio_high)
      ww = int(np.sqrt(targetArea * aspectR) + 0.5)
      hh = int(np.sqrt(targetArea / aspectR) + 0.5)
      if self.rng.uniform() < 0.5:
        ww, hh = hh, ww
      if hh <= h and ww <= w:
        x1 = 0 if w == ww else self.rng.randint(0, w - ww)
        y1 = 0 if h == hh else self.rng.randint(0, h - hh)
        return [CropTransform(y1, x1, hh, ww)]

  def _augment(self, img, params):
    if params:
      for param in params:
        img = param.apply_image(img)
    return img

  def _augment_coords(self, coords, params):
    if params:
      for param in params:
        coords = param.apply_coords(coords)
    return coords

def box_to_point8(boxes):
  """
  Args:
    boxes: nx4
  Returns:
    (nx4)x2
  """
  b = boxes[:, [0, 1, 2, 3, 0, 3, 2, 1]]
  b = b.reshape((-1, 2))
  return b

def point8_to_box(points):
  """
  Args:
    points: (nx4)x2
  Returns:
    nx4 boxes (x1y1x2y2)
  """
  p = points.reshape((-1, 4, 2))
  minxy = p.min(axis=1)   # nx2
  maxxy = p.max(axis=1)   # nx2
  return np.concatenate((minxy, maxxy), axis=1)

def normalize_boxes_classes(boxes, img_shape, classes, orig_classes=None):
  """
    Args:
      nx4 boxes (x1y1x2y2)
      img_shape (h,w)
    Returns:
      nx4 normalized, cliped boxes
      in (y1x1y2x2) format
      as it is required by the object detection
      pipeline
  """
  # Clip and normalize the values
  boxes = np.maximum(boxes, 0.0)
  h, w = map(float, img_shape)
  boxes[:, [0,2]] = np.minimum(boxes[:, [0,2]], w)/w
  boxes[:, [1,3]] = np.minimum(boxes[:, [1,3]], h)/h

  # Remove the empty boxes
  hs = boxes[:, 3] - boxes[:, 1]
  ws = boxes[:, 2] - boxes[:, 0]
  assert(all(hs >= 0) and all(ws >= 0))
  boxes = boxes[(hs > 0) & (ws > 0)]
  classes = classes[(hs >0) & (ws >0)]
  if orig_classes is not None:
    orig_classes = orig_classes[(hs>0) & (ws>0)]
  assert(len(classes) == len(boxes)), 'classes and boxes do not have same sizes %d, %d resp' % (len(classes), len(boxes))

  # Convert the format (x1y1x2y2) ==> (y1x1y2x2)
  return boxes[:, [1, 0, 3, 2]], classes, orig_classes

def normalize_boxes(boxes, img_shape):
  """
    Args:
      nx4 boxes (x1y1x2y2)
      img_shape (h,w)
    Returns:
      nx4 normalized, cliped boxes
      in (y1x1y2x2) format
      as it is required by the object detection
      pipeline
  """
  # Clip and normalize the values
  boxes = np.maximum(boxes, 0.0)
  h, w = map(float, img_shape)
  boxes[:, [0,2]] = np.minimum(boxes[:, [0,2]], w)/w
  boxes[:, [1,3]] = np.minimum(boxes[:, [1,3]], h)/h

  # Remove the empty boxes
  hs = boxes[:, 3] - boxes[:, 1]
  ws = boxes[:, 2] - boxes[:, 0]
  assert(all(hs >= 0) and all(ws >= 0))
  boxes = boxes[(hs > 0) & (ws > 0)] #TODO BUG remove corresponding classes!!!

  # Convert the format (x1y1x2y2) ==> (y1x1y2x2)
  return boxes[:, [1, 0, 3, 2]]

def augmentors(is_training):
  if is_training:
    return [
      # GoogleNetResize(), Note: omitted googlenetresize for supportset
      imgaug.Resize((300, 300), interp=cv2.INTER_CUBIC),
#      imgaug.RandomOrderAug(
#          [imgaug.BrightnessScale((0.6, 1.4), clip=False),
#          imgaug.Contrast((0.6, 1.4), clip=False),
#          imgaug.Saturation(0.4, rgb=False),
#          # rgb-bgr conversion for the constants copied from fb.resnet.torch
#          imgaug.Lighting(0.1,
#                eigval=np.asarray(
#                        [0.2175, 0.0188, 0.0045][::-1]) * 255.0,
#                eigvec=np.array(
#                        [[-0.5675, 0.7192, 0.4009],
#                         [-0.5808, -0.0045, -0.8140],
#                         [-0.5836, -0.6948, 0.4203]],
#                         dtype='float32')[::-1, ::-1]
#                )]),
      imgaug.Flip(horiz=True)]
  else:
    return [imgaug.Resize((300, 300), cv2.INTER_CUBIC)]

def augment_query_images(ds, is_training, use_supportset=True):
  #if not is_training:
  #  return ds
  #query_img, query_boxes, query_classes, supportset_imgs, supportset_boxes
  def mapf(points):
    if points.dtype != np.float32:
      points = np.array(points, dtype=np.float32)
    return box_to_point8(points)
  ds = MapDataComponent(ds, mapf, 1)
  augs = []
  if is_training:
    augs = [imgaug.Flip(horiz=True)]
    ds = AugmentImageComponents(ds, augs, index=(0,), coords_index=(1,))
  def unmapf(ds):
    img_shape = ds[0].shape[:2]
    boxes = point8_to_box(ds[1])
    classes = ds[2]
    orig_classes = None
    if use_supportset:
      orig_classes = ds[3]
    (nboxes,classes,orig_classes) = normalize_boxes_classes(boxes, img_shape, classes, orig_classes)
    try:
      img = ds[0].astype(np.float32)
      img = img[..., ::-1]
      if use_supportset:
        return [img] + [nboxes] + [classes] + [orig_classes] + [ds[4]] + [ds[5]]
      return [img] + [nboxes] + [classes]
    except:
      print 'exception 1'
      from IPython import embed;embed()
  ds = MapData(ds, unmapf)
  return ds

def augment_supportset(ds, is_training, k_shot, n_way, sample_boxes=True):
  #query_img, query_boxes, query_classes, supportset_imgs, supportset_boxes
  def mapf(ds):
    query_img, query_boxes, query_classes, query_orig_classes, supportsetimg_list, supportset_points_list = ds
    points8_list = []
    for points in supportset_points_list:
      if points.dtype != np.float32:
        points = np.array(points, dtype=np.float32)
      points8_list.append(box_to_point8(points))
    return [query_img] + [query_boxes] + [query_classes] + [query_orig_classes] + supportsetimg_list + points8_list
  ds = MapData(ds, mapf)
  augs = augmentors(is_training)

  for i in range(k_shot*n_way):
    ds = AugmentImageComponents(ds, augs, index=(4+i,), coords_index=(4+i+k_shot*n_way,))

  def unmapf(ds):
    points8_list = ds[4+n_way*k_shot:]
    img_shape = ds[4].shape[:2] #Note that all images have the same shape in supportset
    points_list = []
    for points in points8_list:
      boxes = point8_to_box(points)
      nboxes = normalize_boxes(boxes, img_shape)
      points_list.append(nboxes)
    imgs = np.stack(ds[4:4+n_way*k_shot]).astype(np.float32)
    #convert BGR --> RGB
    imgs = imgs[..., ::-1]
    try:
      if sample_boxes:
        all_boxes = []
        for points in points_list:
          if points.shape[0] == 0:
            all_boxes.append(np.ones((1,4), dtype=np.float32)*2) #this will result in zero pad in tf.image.crop_and_resize later
          else:
            sample_idx = np.random.randint(0, points.shape[0])
            all_boxes.append(points[sample_idx].reshape((1,4)))

            boxes = np.stack(all_boxes).reshape((n_way,k_shot,4))
            box2img_indices = np.arange(n_way*k_shot, dtype=np.int32).reshape((n_way, k_shot))
      else:
        return [ds[0]] + [ds[1]] + [ds[2]] + [ds[3]] + [imgs] + [points_list]
      return [ds[0]]+ [ds[1]] + [ds[2]] + [ds[3]] + [imgs] + [boxes] + [box2img_indices]
    except:
      print '-'*60
      traceback.print_exc(file=sys.stdout)
      print '-'*60
      print 'all_points'
      for points in points_list:
        print points.shape
      print 'exception 2'
      from IPython import embed;embed()
  ds = MapData(ds, unmapf)
  return ds

