from data_sampler import DataSampler
from IPython import embed

detection_root = '../data/detection-data/det/ILSVRC2014_processed/'
cls_root = '../data/detection-data/cls/boxed_imagenet_processed/'
split = 'os_val'
if __name__ == '__main__':
  sampler = DataSampler(
              detection_root,
              cls_root,
              split,
              n_way=3,
              k_shot=5,
              selection_prob=1.)

  data = sampler.next()
  embed()

