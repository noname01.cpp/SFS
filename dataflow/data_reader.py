import cPickle as pickle
import os.path as osp
from easydict import EasyDict
import numpy as np

class RandomIterator:
  """
  iterates over [0..N) and returns a number of random indices
  Args:
    N: number of indices to iterate on
  """
  def __init__(self, N, seed=None):
    assert N>=0, 'N should be greater than zero'
    self.indices = range(N)
    self.N = N
    self.rng = np.random.RandomState(seed)
    self.current_idx = 0
    self.reset()

  def reset(self, seed=None):
    self.rng = np.random.RandomState(seed)
    self.current_idx = 0
    self.indices = range(self.N)
    self._shuffle()


  def _shuffle(self):
    """
    Does the shuffling operation. might be changed!
    """
    self.rng.shuffle( self.indices)

  def getNextIndices(self, num=1):
    """
    returns `num` random indices
    Note: assumes num <= N
    """
    assert num > 0, 'requested number (%d) should be greater than zero' % num
    assert num <= self.N, 'too many items requested N is %d, num requested is %d' % (self.N, num)
    ret_indices = []
    if self.current_idx + num <= self.N:
      ret_indices += self.indices[self.current_idx:self.current_idx+num]
      self.current_idx += num
    else:
      ret_indices += self.indices[self.current_idx:]
      self._shuffle()
      num_remained = self.current_idx + num - self.N
      ret_indices += self.indices[:num_remained]
      self.current_idx = num_remained
    return ret_indices

class DataReader:
  """
  reads a database
  """
  def __init__(self, root_path, split='train', ext='.pkl'):
    """
    Args:
      root_path: address of the ImageSet folder where the pkl files are stored
    """
    dataset_path = osp.join( root_path,'ImageSet', split + ext)
    with open( dataset_path , 'rb') as f:
      self.data = EasyDict(pickle.load(f))
    self._check_data()
    self.nr_images = len(self.data.images)

    # number of classes including background class 0
    self.nr_classes = len(self.data.synsets)

    # class indices stores current index to read for each class
    self.class_indices = [ [] for _ in range(self.nr_classes) ]
    # fill them
    self._fill_class_indices()

    # random iterators for each class/roi
    self.class_iterators = [ RandomIterator(len(x)) for x in self.class_indices ]
    # random iterator for each image
    self.image_iterator = RandomIterator(self.nr_images)

  def reset_image_iterator(self, seed=None):
    self.image_iterator.reset(seed)

  def reset_class_iterators(self, seeds=None):
    """resets class iterators

       Args:
         seeds:
          1- can be list of seeds equal to the number of classes `self.nr_classes`. Then, each class iterator will be reset with its corresponding seed.
          2- can be a single number. Then, all of the class iterators will be reset by that seed.
          3- None, then all class iterators will be reset with random seed.
    """
    if seeds == None:
      for class_iterator in self.class_iterators:
        class_iterator.reset()
    elif isinstance(seeds, list):
      if len(seeds) == self.nr_classes:
        for seed, class_iterator in zip(seeds, self.class_iterators):
          class_iterator.reset(seed)
      else:
        raise ValueError("seeds len is not the same as number of classes")
    elif isinstance(seeds, int):
      for class_iterator in self.class_iterators:
        class_iterator.reset(seed=seeds)
    else:
      raise ValueError("Invalid seeds type")

  def _check_data(self):
    """
    Checks for the validity of data.

    Note: Somee tests (e.g ensuring the values of the keys are of list type )
    are not yet implemented.
    """
    assert 'synsets' in self.data.keys(), 'dataset does not have "synsets" key'
    assert 'images' in self.data.keys(), 'dataset does not have "images" key'
    assert 'rois' in self.data.keys(), 'dataset does not have "rois" key'
    assert len(self.data.images) == len(self.data.rois), 'rois and images length mismatch'

  def _fill_class_indices(self):
    """
    for each class fills rois ( or images) indices of that class in `self.class_indices`
    Note: there are 3 cases
      1) classification with no boxes.
      2) classification with multiple boxes (TODO: add only 1 image if >1 class-objects in an image? )
      3) detection we add all boxes (so, one image which have 2 different boxes of the same class 
         will be added twice and hence its selection probability is mutliplied by 2)
    """
    for i, roi in enumerate(self.data.rois):
      # 1) classification with no boxes. 
      if len(roi.classes) == 0 and 'image_class' in roi.keys():
        class_idx = roi.image_class
        self.class_indices[ class_idx ].append( i )
      # 2) classification with multiple boxes (TODO: add only 1 image if >1 class-objects in an image? )
      elif 'image_class' in roi.keys():
        idx_set = set(roi.classes)
        for class_idx in idx_set:
          self.class_indices[ class_idx ].append( i )
      # 3) detection we add all boxes (so, one image which have 2 different boxes of the same class will be added twice and hence its selection probability is mutliplied by 2)
      else:
        for class_idx in roi.classes:
          self.class_indices[ class_idx ].append( i )

  def get_class_indices(self):
    return self.class_indices

  def next_images_data(self, num):
    """
    returns next `num` random images and rois
    """
    indices = self.image_iterator.getNextIndices(num = num)
    images = [self.data.images[i] for i in indices]
    rois   = [self.data.rois[i] for i in indices]
    return images, rois

  def next_class_data(self, class_idx, num ):
    """
    Selects `num` items from class `class_idx`
    Note: here we assume the num is too much less than the items in class_idx so if we reach the last index in the array we need to start from the beginning only once.
    """

    assert class_idx < self.nr_classes, "class_idx (%d) should be less than the number of classes (%d)! " % (class_idx, self.nr_classes)
    assert class_idx >= 0, "class_idx (%d) should be greater than or equal to zero!" % class_idx
    assert num > 0, "number of selected data (%d) should be greater than zero!" % num
    assert len(self.class_indices[class_idx]) >= num, "this class (%d) does not have enought items (%d)!" % (class_idx, num)

    current_class_index = self.class_indices[class_idx]

    indices = self.class_iterators[class_idx].getNextIndices(num = num)
    current_indices = [current_class_index[i] for i in indices]

    images =  [self.data.images[i] for i in current_indices]
    rois   =  [self.data.rois[i] for i in current_indices]
    return images, rois


