from finetune_reader import FinetuneReader
from tensorpack.utils.utils import fix_rng_seed
import numpy as np
import cv2
import os
import shutil

def draw_anns(img, boxes, tags):
    ps = 50
    h, w = img.shape[:2]
    img = np.pad(img, ((ps,ps),(ps,ps),(0,0)), 'constant')
    for box, tag in zip(boxes, tags):
      y1 = int(box[0]*h+ps)
      x1 = int(box[1]*w+ps)
      y2 = int(box[2]*h+ps)
      x2 = int(box[3]*w+ps)
      cv2.putText(img, str(tag), tuple((x1+10,y1+10)), cv2.FONT_HERSHEY_SIMPLEX, .4, (0,255,0))
      cv2.rectangle(img, (x1,y1), (x2,y2), (0,0,255), 2)
    return img

def viz(path, img, boxes, class_indices):
    img = img[...,::-1]
    img = draw_anns(img, boxes, class_indices)
    cv2.imwrite(path, img)

if __name__ == '__main__':
#    np.random.seed(7)
    reader = FinetuneReader(
            '../data/detection-data/det/ILSVRC2014_processed', '../data/detection-data/cls/boxed_imagenet_processed', 'os_val', n_way=2, k_shot=3, resize_widths=[450,300,224])
    viz_path = './finetune_viz'
    if os.path.exists(viz_path):
        shutil.rmtree(viz_path)
    os.mkdir(viz_path)
    for i in range(3):
        ds_path = os.path.join(viz_path, 'ds_'+str(i))
        os.mkdir(ds_path)
        reader.next_classes()
        reader.change_support_data()
        reader.change_query_data()
        for j in range(20):
          if j == 10:
            reader.change_support_data()
          ds = reader.support_data()
          npath = os.path.join(ds_path, 'img_'+str(j)+'.png')
          viz(npath, ds[0], ds[1], ds[2])
        for k in range(10):
          ds = reader.query_data()
          npath = os.path.join(ds_path, 'query_'+str(k)+'.png')
          viz(npath, ds[0], ds[1], ds[2])
          reader.change_query_data()
    reader = FinetuneReader(
            '../data/detection-data/det/ILSVRC2014_processed', '../data/detection-data/cls/boxed_imagenet_processed', 'os_val', n_way=1, k_shot=3, resize_widths=[450,300,224])
    reader.next_classes()
    reader.change_support_data()
    for i in range(30):
      reader.change_query_data()
      ds = reader.query_data()
      print('query at {} iter is {}'.format(i,ds[3]))
      reader.next_classes()

    ### testing reset states
    for i in range(2):
      reader.reset_next_classes(seed=7)
      print('Classes to choose: {}'.format(reader.ds.classes_to_choose))

    for i in range(3):
      reader.reset_query_data(classes_seeds=7, selector_seed=13)
      reader.change_query_data()
      ds = reader.query_data()
      print('query is {}'.format(ds[3]))
      reader.change_query_data()
      ds = reader.query_data()
      print('query is {}'.format(ds[3]))


