from data_reader import DataReader
import numpy as np
from itertools import compress

class DataSampler:
  def __init__(self,
      detection_root, classification_root, split,
      n_way=1, k_shot=1, selection_prob=1.):
    self.cls_reader = DataReader( root_path=classification_root, split=split)
    self.det_reader = DataReader( root_path=detection_root, split=split)
    self.n_way = n_way
    self.k_shot = k_shot
    self.prob = selection_prob
    self.all_classes = set(range(1, self.cls_reader.nr_classes)) # here we assume 0 is background class

  def _next(self, det_img, det_roi):
    det_classes = set(det_roi.classes)
    det_classes = self.all_classes & det_classes
    sel = np.random.uniform(size=len(det_classes)) < self.prob #selection array
    det_classes = set( compress( det_classes, sel ) ) # final selected detection classes
    if len(det_classes) > self.n_way: # select randomly?
      det_classes = set(np.random.choice( list(det_classes), self.n_way, replace=False))

    excluded = set(det_roi.classes) - det_classes # not to choose from these classes
    other_classes = self.all_classes - det_classes - excluded # other classes to choose from
    num_of_other = self.n_way - len(det_classes)
    other_classes = set( np.random.choice( list(other_classes), num_of_other, replace=False) ) # randomly chosen from other classes
    classes_to_choose = list(det_classes.union(other_classes) )
    assert len(classes_to_choose) == self.n_way, 'number of classes to choose (%d) is not equal to n_way (%d)' % ( len(classes_to_choose), self.n_way )
    np.random.shuffle(classes_to_choose) #random shuffle classes to choose list
    images = []
    rois = []
    labels = []
    det_classes_list = list(det_classes)
    np.random.shuffle(det_classes_list) # random shuffle detection class orders
    for class_idx in classes_to_choose:
      cls_imgs, cls_rois = self.cls_reader.next_class_data( class_idx, self.k_shot)
      images.append( cls_imgs )
      rois.append( cls_rois )

    n_det_boxes = []
    n_det_classes = []
    n_det_orig_classes = []
    for cls, box in zip(det_roi['classes'], det_roi['boxes']):
      if cls in classes_to_choose:
        n_det_classes.append(classes_to_choose.index(cls) + 1) #Note classes start from 1
        n_det_orig_classes.append(cls)
        n_det_boxes.append(box)

    det_rois = dict(classes=np.array(n_det_classes, dtype=np.uint32),
                    orig_classes=np.array(n_det_orig_classes, dtype=np.uint32),
                    boxes=np.array(n_det_boxes, dtype=np.uint32).reshape((-1, 4)))
    #TODO, changing classification rois classes to match the detections
    for n in range(self.n_way):
      for k in range(self.k_shot):
        rois[n][k]['classes'] = np.ones_like(rois[n][k]['classes'])*n
        #sample one box per image for support set to make things easier!
        #sample_idx = np.random.randint(0, rois[n][k]['boxes'].shape[0])
        #rois[n][k]['classes'] = rois[n][k]['classes'][sample_idx].reshape((1,))
        #rois[n][k]['boxes'] = rois[n][k]['boxes'][sample_idx].reshape((1,4))

    #support_box2image_indices = np.arange(self.n_way*self.k_shot).reshape((self.n_way,self.k_shot))

    return images, rois, det_rois

  def next(self):
    det_imgs, det_rois = self.det_reader.next_images_data(num=1)
    cls_imgs, cls_rois, det_rois = self._next(det_imgs[0], det_rois[0])

    return dict(query_img=det_imgs[0], query_rois=det_rois, supportset_imgs=cls_imgs, supportset_rois=cls_rois)
