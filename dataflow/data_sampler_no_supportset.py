from data_reader import DataReader
import random
import numpy as np
from itertools import compress

class DataSamplerNoSS:
  def __init__(self,
      detection_root, split):
    self.det_reader = DataReader( root_path=detection_root, split=split)
    self.all_classes = set(range(1, self.det_reader.nr_classes)) # here we assume 0 is background class

  def next(self):
    det_imgs, det_rois = self.det_reader.next_images_data(num=1)
    return dict(query_img=det_imgs[0], query_rois=det_rois[0])
