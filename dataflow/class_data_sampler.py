from data_reader import DataReader
import numpy as np
from itertools import compress
from data_reader import RandomIterator

class ClassDataSampler:
  def __init__(self,
      detection_root, classification_root, split,
      n_way=1, k_shot=1, selection_prob=1.):
    self.cls_reader = DataReader( root_path=classification_root, split=split)
    self.det_reader = DataReader( root_path=detection_root, split=split)
    self.n_way = n_way
    self.k_shot = k_shot
    self.prob = selection_prob
    self.all_classes = set(range(1, self.cls_reader.nr_classes)) # here we assume 0 is background class
    self.supportset_class_lengths = [len(x) for x in self.cls_reader.class_indices]
    self.query_class_lengths      = [len(x) for x in self.det_reader.class_indices]
    self.class_iterator           = RandomIterator(len(self.all_classes))
    self.classes_to_choose = None
    self.query_selector_rng = np.random.RandomState()

  def next_classes(self):
    all_classes = list(self.all_classes)
    classes = set()
    num_remained = self.n_way
    while len(classes) != self.n_way:
      next_class_indices = self.class_iterator.getNextIndices(num = num_remained)
      next_classes = [all_classes[i] for i in next_class_indices]
      classes |= set(next_classes)
      num_remained = self.n_way - len(classes)

    self.classes_to_choose = list(classes)
    return self.classes_to_choose

  def reset_classes_to_choose_iterator(self, seed=None):
    self.class_iterator.reset(seed)

  def reset_query_selector(self, seed=None):
    self.query_selector_rng = np.random.RandomState(seed)

  def reset_query_class_iterators(self, seeds=None):
    if isinstance(seeds, list):
      if len(seeds) == len( self.all_classes):
        seeds = [None] + seeds #Background is not important
    self.det_reader.reset_class_iterators(seeds)

  #def reset_query_class_iterators(self, seeds=None):
  #  self._reset_class_iterators(self.det_reader, seeds)

  def reset_supportset_class_iterators(self, seeds=None):
    if isinstance(seeds, list):
      if len(seeds) == len( self.all_classes):
        seeds = [None] + seeds
    self.cls_reader.reset_class_iterators(seeds)
  #  self._reset_class_iterators(self.cls_reader, seeds)

  def get_num_query_images(self, query_orig_class):
    """
    returns the number of query images of `query_orig_class` index.
    """
    return self.query_class_lengths[query_orig_class]

  def get_num_support_images(self, support_orig_class):
    """
    returns the number of supportset images of `support_orig_class` index.
    """
    return self.supportset_class_lengths[support_orig_class]

  def get_num_classes(self):
    """
    returns number of classes without background
    """
    return len(self.all_classes)

  def sample_supportset_from_classes(self, classes_to_choose):
    images = []
    rois   = []
    for class_idx in classes_to_choose:
      cls_imgs, cls_rois = self.cls_reader.next_class_data( class_idx, self.k_shot)
      images.append( cls_imgs )
      rois.append( cls_rois )

    #TODO, changing classification rois classes to match the detections
    for n in range(self.n_way):
      for k in range(self.k_shot):
        rois[n][k]['classes'] = np.ones_like(rois[n][k]['classes'])*n
    return dict(supportset_imgs=images, supportset_rois=rois)

  def sample_single_query_from_classes(self, classes_to_choose):
    query_class = self.query_selector_rng.choice(classes_to_choose, 1)[0]
    det_imgs, det_rois = self.det_reader.next_class_data(query_class, num=1)
    det_img=det_imgs[0]
    det_roi=det_rois[0]
    n_det_boxes = []
    n_det_classes = []
    n_det_orig_classes = []
    for cls, box in zip(det_roi['classes'], det_roi['boxes']):
      if cls in classes_to_choose:
        n_det_classes.append(classes_to_choose.index(cls) + 1) #Note classes start from 1
        n_det_orig_classes.append(cls)
        n_det_boxes.append(box)

    det_rois = dict(classes=np.array(n_det_classes, dtype=np.uint32),
                    orig_classes=np.array(n_det_orig_classes, dtype=np.uint32),
                    boxes=np.array(n_det_boxes, dtype=np.uint32).reshape((-1, 4)))
    return dict(query_img=det_imgs[0], query_rois=det_rois)

