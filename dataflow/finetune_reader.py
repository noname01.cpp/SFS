from tensorpack_utils import normalize_boxes_classes, normalize_boxes
from class_data_sampler import ClassDataSampler
import numpy as np
import cv2
import os
import os.path as osp

#TODO: thread safety?
class ClassDataLoader():
  def __init__(self, det_root, cls_root, split,
              n_way=1, k_shot=1, selection_prob=1.,
              support_resize_size=(300,300)):
    self.det_root = det_root
    self.cls_root = cls_root
    self.n_way = n_way
    self.k_shot = k_shot
    self.split = split
    self.support_resize_size = support_resize_size
    self.sampler = ClassDataSampler(det_root, cls_root, split, n_way, k_shot, selection_prob)
    self.classes_to_choose = None
    self.support_dict = None
    self.query_dict   = None

  def size(self):
    return self.sampler.det_reader.nr_images

  def _load_supportset_images(self, sample):
    imgs = []
    for way in xrange(self.n_way):
      for k in xrange(self.k_shot):
        img_path = osp.join(self.cls_root, 'Images', sample['supportset_imgs'][way][k])
        img = cv2.imread(img_path, cv2.IMREAD_COLOR)
        imgs.append(img)
    return imgs

  def _load_det_img(self, sample):
    img_path = osp.join(self.det_root, 'Images', sample['query_img'])
    img = cv2.imread(img_path, cv2.IMREAD_COLOR)
    return img

  def _unpack_support_roi_boxes(self, sample):
    supportset_boxes = []
    for way in xrange(self.n_way):
      for k in xrange(self.k_shot):
        supportset_boxes.append(sample['supportset_rois'][way][k]['boxes'])
    return supportset_boxes

  def get_num_classes(self):
    return self.sampler.get_num_classes()

  def get_num_query_images(self, query_orig_class):
    return self.sampler.get_num_query_images(query_orig_class)

  def get_num_support_images(self, support_orig_class):
    return self.sampler.get_num_support_images(support_orig_class)

  def next_classes(self):
    self.classes_to_choose = self.sampler.next_classes()

  def reset_next_classes(self, seed=None):
    self.sampler.reset_classes_to_choose_iterator(seed)

  def next_supportset(self):
    self.support_dict = self.sampler.sample_supportset_from_classes(self.classes_to_choose)
    self.supportset_imgs = self._load_supportset_images(self.support_dict)
    self.supportset_boxes = self._unpack_support_roi_boxes(self.support_dict)

    n_supportset_boxes = []
    for supportset_box, supportset_img in zip(self.supportset_boxes, self.supportset_imgs):
      n_supportset_boxes.append(normalize_boxes(supportset_box, supportset_img.shape[:2]).astype(np.float32))
    for i in range(len(self.supportset_imgs)):
      self.supportset_imgs[i] = cv2.resize(self.supportset_imgs[i], self.support_resize_size, interpolation=cv2.INTER_NEAREST)
    # STACK and BGR --> RGB for Supportset images
    self.supportset_imgs = np.stack(self.supportset_imgs).astype(np.float32)[...,::-1]
    self.supportset_boxes = n_supportset_boxes
    return self.supportset_imgs, self.supportset_boxes

  def reset_next_supportset(self, seeds=None):
    self.sampler.reset_supportset_class_iterators(seeds)

  def next_query(self):
    self.query_dict = self.sampler.sample_single_query_from_classes(self.classes_to_choose)
    self.query_img  = self._load_det_img(self.query_dict)
    query_boxes = self.query_dict['query_rois']['boxes']
    query_classes  = self.query_dict['query_rois']['classes']
    query_orig_classes = self.query_dict['query_rois']['orig_classes']
    (self.query_boxes, self.query_classes, self.query_orig_classes) = normalize_boxes_classes(query_boxes, self.query_img.shape[:2], query_classes, query_orig_classes)
    self.query_boxes = self.query_boxes.astype(np.float32)
    self.query_img = self.query_img[...,::-1].astype(np.float32)

  def reset_next_query(self, classes_seeds=None, selector_seed=None):
    self.sampler.reset_query_class_iterators(classes_seeds)
    self.sampler.reset_query_selector(selector_seed)

  def get_data(self):
    supportset_imgs = self.supportset_imgs
    query_img = self.query_img
    query_boxes = self.query_boxes
    query_classes = self.query_classes
    query_orig_classes = self.query_orig_classes
    supportset_boxes = self.supportset_boxes
    return dict(query_img=query_img, query_boxes=query_boxes, query_classes=query_classes, query_orig_classes=query_orig_classes, supportset_boxes=supportset_boxes, supportset_imgs=supportset_imgs)

def flip_horizontal_box(boxes):
    boxes[:,[1,3]] = 1. - boxes[:,[1,3]]
    return boxes[:,[0,3,2,1]]

class FinetuneReader():
    def __init__(self, det_root, cls_root, split,
               n_way=1, k_shot=1, selection_prob=1.,
               resize_widths=[321, 241, 161]):
      self.det_root = det_root
      self.cls_root = cls_root
      self.split    = split
      self.n_way    = n_way
      self.k_shot   = k_shot
      self.resize_widths = resize_widths

      self.ds = ClassDataLoader(det_root, cls_root, split, n_way=n_way,
                                 k_shot=k_shot, selection_prob=selection_prob)
      self.base_ds = None

    def size(self):
      return self.ds.size()

    def get_num_classes(self):
      return self.ds.get_num_classes()

    def get_num_query_images(self, query_orig_class):
      return self.ds.get_num_query_images(query_orig_class)

    def get_num_support_images(self, support_orig_class):
      return self.ds.get_num_support_images(support_orig_class)


    def next_classes(self):
      """
      self.base_ds has [query_img, query_boxes, query_classes, query_orig_classes,
                        support_imgs, supportset_boxes]
          - query_img is [W,H,3] numpy array representing query image.
          - query_boxes is a [?,4] numpy array representing query groundtruth boxes in
              normalized coordinates.
          - query_classes is [?] numpy array representing the class of each box in this 
              dataset sample for query image.
          - query_orig_classes is [?] numpy array representing the original class of each box in the query image.
          - support_imgs is [n_way*k_shot, 300, 300, 3] numpy array representing n_way*k_shot images in the support set.
          - support_boxes is [n_way, k_shot, 4] numpy array representing one bounding box per support set image in normalized coordinates.

      Note: boxes are in (y1x1y2x2) format.
      """
      self.ds.next_classes()

    def reset_next_classes(self, seed=None):
      self.ds.reset_next_classes(seed)

    def query_data(self):
      """
      """
      data = self.ds.get_data()
      query_img = data['query_img']
      query_boxes = data['query_boxes']
      query_classes = data['query_classes']
      query_orig_classes = data['query_orig_classes']
      return [query_img, query_boxes, query_classes, query_orig_classes]

    def change_support_data(self):
      self.ds.next_supportset()

    def reset_support_data(self, seeds=None):
      self.ds.reset_next_supportset(seeds)

    def change_query_data(self):
      self.ds.next_query()

    def reset_query_data(self, classes_seeds=None, selector_seed=None):
      self.ds.reset_next_query(classes_seeds, selector_seed)

    def original_support_data(self):
      data = self.ds.get_data()
      support_imgs  = data['supportset_imgs']
      support_boxes = data['supportset_boxes']
      labels = []
      for n in range(self.n_way):
        for k in range(self.k_shot):
          boxes = support_boxes[n*self.k_shot + k]
          labels.append(np.ones((boxes.shape[0]), dtype=np.float32) * (n+1))
      return [support_imgs.copy(), support_boxes, labels]

    def support_data(self):
      """
      This function takes the self.base_ds and selects one image from its support set,
      randomly resizes it and returns its corresponding bounding box and label.

      Note: self.next_dataset should be called before calling this
      """
      data = self.ds.get_data()
      support_img = data['supportset_imgs']
      support_boxes = data['supportset_boxes']
      n = np.random.randint(0,self.n_way)
      k = np.random.randint(0,self.k_shot)
      width = np.random.choice(self.resize_widths)
      flip = np.random.choice([0,1])
      img = support_img[n*self.k_shot + k].copy()
      boxes = support_boxes[n*self.k_shot + k].copy()
      labels = np.ones((boxes.shape[0]), dtype=np.float32)*(n+1)
      img = cv2.resize(img, (width, width), interpolation=cv2.INTER_NEAREST)
      if flip == 1:
          img = cv2.flip(img, 1) #horizontal flip
          boxes = flip_horizontal_box(boxes)
      return [img, boxes, labels]
